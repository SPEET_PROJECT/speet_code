#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 09 19:27:51 2018
@author: Adrian

Updated on Wed April 11 19:52:00 2018
@author: Alan
"""

import os
import time

from categorical_study.Plot_Categorical import plot_categorical_generator
from classifier.Train_Predict_SVM_unbalanced import train_predict_svm_unbalanced
from clustering.clustering import clustering
from clustering.visualization import visualization
from data_average.data_average_histograms import data_average_histograms
from data_preparation.data_preparation2 import data_preparation2
from data_preparation.data_preparation import data_preparation
from utils.utils import directories_generator


def main():
    path = '../exports/'
    absolute_path = os.getcwd()

    institution = "University"
    degree = "1"
    degree_str = degree

    directories_generator(institution, degree_str, path)
    path += institution + degree_str

    # First data processing: Reads all the data and build the data frames
    first_time = time.clock()
    data_frame, segmentation_variables, subjects_123, student_id, df_at, subjects_12, segmentation_variables_2, df_at2, segmentation_variables_3, df_at3, segmentation_variables_4, subjects_1 = data_preparation(absolute_path, degree)
    print("data_preparation time: " + str(time.clock() - first_time) + " seconds")

    start_time = time.clock()
    data_frame, transform, labels = clustering(segmentation_variables, data_frame, subjects_123, path)
    print("clustering time: " + str(time.clock() - start_time) + " seconds")

    start_time = time.clock()
    visualization(transform, labels, path)
    print("visualization time: " + str(time.clock() - start_time) + " seconds")

    start_time = time.clock()
    data_average_histograms(data_frame, subjects_123, path)
    print("data_average_histograms time: " + str(time.clock() - start_time) + " seconds")

    # Second data processing: Adds categorical variables (in order to characterize the clusters)
    start_time = time.clock()
    categorical, categorical_1, categorical_12, categorical_123, df_final = data_preparation2(data_frame, subjects_1,
                                                                                              subjects_12)
    print("data_preparation2 time: " + str(time.clock() - start_time) + " seconds")

    print("df_final.size: " + str(df_final.size))

    start_time = time.clock()
    plot_categorical_generator(df_final, categorical, labels, path)
    print("plot_categorical_generator time: " + str(time.clock() - start_time) + " seconds")

    start_time = time.clock()
    train_predict_svm_unbalanced(labels, segmentation_variables_2, segmentation_variables_3, segmentation_variables_4,
                               categorical, categorical_1, categorical_12, categorical_123, path)
    print("train_predict_svm_unbalanced time: " + str(time.clock() - start_time) + " seconds")

    print("Full execution time: " + str(time.clock() - first_time) + " seconds")

    return


if __name__ == "__main__":
    main()
