import os


def directories_generator(institution, degree, path):
    """
    :param institution: Institution
    :param degree: Degree or course.
    :param path: General path where the temporary folder will be place.

    :return temporary_folder: Temporary folder where results will be placed.
    """
    directory = path
    if not os.path.exists(directory):
        os.makedirs(directory)

    directory = path + '/' + institution + degree + '/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    directory = path + '/' + institution + degree + '/Categorical_Study'
    if not os.path.exists(directory):
        os.makedirs(directory)

    directory = path + '/' + institution + degree + '/Classification'
    if not os.path.exists(directory):
        os.makedirs(directory)

    directory = path + '/' + institution + degree + '/Clusters'
    if not os.path.exists(directory):
        os.makedirs(directory)

    return


def get_hex_color(color):
    """
    :param color: Color name.
    :return: Hex code.
    """
    colors = {'red': '#DC3545', 'green': '#28A745', 'blue': '#007BFF'}
    return colors[color]
