# -*- coding: utf-8 -*-
"""
Created on Wed Sep 06 10:16:28 2017
@author: SPEET

Updated on Wed April 11 19:52:00 2018
@author: Alan
"""
import numpy as np


def label_reorder(data_frame, subjects):
    """
    In this function we assign always the same labels to clusters:
    Average Students (av) = 0
    Excellent students (ex) = 1
    Low-performance Students (lp) = 2

    :param data_frame: Dataframe with all the marks with 2 features and labels
    :param subjects: List with all the subject names
    :return: Data frame with all the marks with 2 features and
             labels reordered and array with the ground truth clusters reordered
    """
    df_location_subjects_av, df_location_subjects_ex, df_location_subjects_lp = data_initialize_reorder(data_frame,
                                                                                                        subjects)
    label_mean_av, label_mean_ex, label_mean_lp = data_mean_by_axis(1,
                                                                    df_location_subjects_av,
                                                                    df_location_subjects_ex,
                                                                    df_location_subjects_lp)
    mean_av = np.mean(label_mean_av)
    mean_ex = np.mean(label_mean_ex)
    mean_lp = np.mean(label_mean_lp)

    lower = min(mean_av, mean_ex, mean_lp)
    higher = max(mean_av, mean_ex, mean_lp)
    label_reordered = [0, 1, 2]

    if higher == mean_ex:
        if lower == mean_av:
            label_reordered = [2, 1, 0]
        elif lower == mean_lp:
            label_reordered = [0, 1, 2]
    elif higher == mean_av:
        if lower == mean_ex:
            label_reordered = [1, 2, 0]
        elif lower == mean_lp:
            label_reordered = [1, 0, 2]
    elif higher == mean_lp:
        if lower == mean_ex:
            label_reordered = [0, 2, 1]
        elif lower == mean_av:
            label_reordered = [2, 0, 1]
    data_frame['Label'].replace([0, 1, 2], label_reordered, inplace=True)

    labels = data_frame['Label'].tolist()

    return data_frame, labels


def data_initialize_reorder(data_frame, subjects):
    """
    :param data_frame: Data frame with all the marks with 2 features and labels.
    :param subjects: List with all the subject names.
    :return: Data frames grouped by clusters
    """
    df_location_subjects_av = data_frame[data_frame['Label'] == 0][subjects]
    df_location_subjects_ex = data_frame[data_frame['Label'] == 1][subjects]
    df_location_subjects_lp = data_frame[data_frame['Label'] == 2][subjects]

    return df_location_subjects_av, df_location_subjects_ex, df_location_subjects_lp


def data_mean_by_axis(axis, df_location_subjects_av, df_location_subjects_ex, df_location_subjects_lp):
    """
    :param axis: 0=X axis (subjects), 1=Y axis (students)
    :param df_location_subjects_av: Processed data frame of Average Students
    :param df_location_subjects_ex: Processed data frame of Excellent students
    :param df_location_subjects_lp: Processed data frame of Low-performance Students
    :return: Mean of the data (just students or subjects) grouped by clusters
    """
    label_mean_av = df_location_subjects_av.mean(axis=axis).values
    label_mean_ex = df_location_subjects_ex.mean(axis=axis).values
    label_mean_lp = df_location_subjects_lp.mean(axis=axis).values

    return label_mean_av, label_mean_ex, label_mean_lp
