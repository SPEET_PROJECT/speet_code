# -*- coding: utf-8 -*-
"""
Created on Wed May 31 09:58:23 2017
@author: SPEET

Updated on Wed April 11 19:52:00 2018
@author: Alan
"""
from matplotlib import pyplot as plt, patches as mpatches
from matplotlib.backends.backend_pdf import PdfPages

from label_reorder.label_reorder import data_initialize_reorder, data_mean_by_axis
from utils.utils import get_hex_color


def data_average_histograms(data_frame, subjects, path):
    """
    In this function we will compute and plot the averages scores of students and subjects

    :param data_frame: Data frame with all the marks with 2 features and labels.
    :param subjects: List with all the subject names.
    :param path: Path where the files will be exported.

    Average Students = 0
    Excellent students = 1
    Low-performance Students = 2

    Subjects -> X axis = 0
    Students -> Y axis = 1
    """
    data_frame.to_csv(path + '/Clusters/ScoresLabels.csv')
    df_location_subjects_av, df_location_subjects_ex, df_location_subjects_lp = data_initialize_reorder(data_frame,
                                                                                                        subjects)

    for axis in range(2):
        label_mean_av, label_mean_ex, label_mean_lp = data_mean_by_axis(axis,
                                                                        df_location_subjects_av,
                                                                        df_location_subjects_ex,
                                                                        df_location_subjects_lp)
        plot_histogram(axis, label_mean_av, label_mean_ex, label_mean_lp, path)

    return


def plot_histogram(axis, av_students_data, ex_students_data, lp_students_data, path):
    """
    Generates one histogram
     of the students average: PDF (Matplotlib).

    :param axis: 0=X axis (subjects), 1=Y axis (students)
    :param av_students_data: Mean of the data (just students or subjects) of Average Students
    :param ex_students_data: Mean of the data (just students or subjects) of Excellent students
    :param lp_students_data: Mean of the data (just students or subjects) of Low-performance Students
    :param path: Path where the files will be exported.
    """
    red = get_hex_color('red')
    blue = get_hex_color('blue')
    green = get_hex_color('green')

    plt.hist(av_students_data, bins=50, range=(0, 10), color=red, alpha=0.7)
    plt.hist(ex_students_data, bins=50, range=(0, 10), color=blue, alpha=0.7)
    plt.hist(lp_students_data, bins=50, range=(0, 10), color=green, alpha=0.7)

    destination_file = path + '/Clusters/average_'

    if axis is 1:
        destination_file = destination_file + 'students.'
        plt.title("AVERAGE SCORE STUDENTS")
    elif axis is 0:
        destination_file = destination_file + 'subjects.'
        plt.title("AVERAGE SCORE SUBJECTS")

    red_patch = mpatches.Patch(color=red, label='Average Students')
    blue_patch = mpatches.Patch(color=blue, label='Excellent Students')
    green_patch = mpatches.Patch(color=green, label='Low-Performance Students')
    plt.legend(handles=[red_patch, blue_patch, green_patch], loc=1)
    plt.ylabel('Number of students')
    plt.xlabel('Scores')
    pp = PdfPages(destination_file + 'pdf')
    plt.savefig(pp, format='pdf', transparent=True)
    plt.show()
    plt.close()
    pp.close()

    return
