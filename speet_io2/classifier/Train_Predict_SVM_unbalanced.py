# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 09:35:07 2017
@author: SPEET

Updated on Wed April 11 19:52:00 2018
@author: Alan
"""
import time
import numpy as np
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


def train_predict_svm_unbalanced(labels, segmentation_variables_2, segmentation_variables_3, segmentation_variables_4,
                                 categorical, categorical_segment_1, categorical_segment_2, categorical_segment_3, path):
    """
    In this function we will train an evaluate 7 SVM classifiers:
        Classification with only subjects of the first course
        Classification with only subjects of the first + second course
        Classification with only subjects of the first + second + third course
        Classification with only Categorical Variables
        Classification with only subjects of the first + Categorical Variables
        Classification with only subjects of the first + second course  + Categorical Variables
        Classification with only subjects of the first + second + third course + Categorical Variables

    :param labels: Array with the ground truth clusters reordered
    :param segmentation_variables_2: 2D array with the marks of the 1st course per student
    :param segmentation_variables_3: 2D array with the marks of the 2nd course per student
    :param segmentation_variables_4: 2D array with the marks of the 3rd course per student
    :param path: Route where the files will be exported

    :return: The 7 classification reports of the classifiers
    """
    selected_kernel = 'linear'
    c_selected = 1
    destination_file = open(path + '/Classification/Results.txt', 'w')
    destination_file.write("Results obtained with unbalanced SVM\n\n")

    destination_file.write("Kernelselected: %s\n\n" % selected_kernel)
    destination_file.write("Cselected: %f\n\n" % c_selected)
    subjects_1_buffer = []
    subjects_1_time = []
    subjects_2_buffer = []
    subjects_2_time = []
    subjects_3_buffer = []
    subjects_3_time = []
    categorical_buffer = []
    categorical_time = []
    combined_1_buffer = []
    combined_1_time = []
    combined_2_buffer = []
    combined_2_time = []
    combined_3_buffer = []
    combined_3_time = []

    for w in range(100):
        # Scores of the 1st course
        start_time = time.time()
        y = labels
        x = segmentation_variables_2

        scaler_1 = StandardScaler()
        x_transform = scaler_1.fit_transform(x)
        x_train, x_test, y_train, y_test = train_test_split(x_transform, y, test_size=0.20, stratify=y)

        clf_1 = svm.SVC(C=c_selected, cache_size=200, class_weight='balanced', coef0=0.0,
                        decision_function_shape='ovr', degree=3, gamma='auto', kernel=selected_kernel,
                        max_iter=-1, probability=True, random_state=None, shrinking=True,
                        tol=0.001, verbose=False)
        clf_1.fit(x_train, y_train)

        subjects_1_buffer.append(clf_1.score(x_test, y_test))
        subjects_1_time.append((time.time() - start_time))

        # Scores of the 1st and 2nd course
        start_time = time.time()
        y = labels
        x = segmentation_variables_3

        scaler_2 = StandardScaler()
        x_transform = scaler_2.fit_transform(x)
        x_train, x_test, y_train, y_test = train_test_split(x_transform, y, test_size=0.20, stratify=y)

        clf_2 = svm.SVC(C=c_selected, cache_size=200, class_weight='balanced', coef0=0.0,
                        decision_function_shape='ovr', degree=3, gamma='auto', kernel=selected_kernel,
                        max_iter=-1, probability=True, random_state=None, shrinking=True,
                        tol=0.001, verbose=False)
        clf_2.fit(x_train, y_train)

        subjects_2_buffer.append(clf_2.score(x_test, y_test))
        subjects_2_time.append((time.time() - start_time))

        # Scores of the 1st, 2nd and 3rd course
        start_time = time.time()
        y = labels
        x = segmentation_variables_4

        scaler_3 = StandardScaler()
        x_transform = scaler_3.fit_transform(x)

        x_train, x_test, y_train, y_test = train_test_split(x_transform, y, test_size=0.20, stratify=y)

        clf_3 = svm.SVC(C=c_selected, cache_size=200, class_weight='balanced', coef0=0.0,
                        decision_function_shape='ovr', degree=3, gamma='auto', kernel=selected_kernel,
                        max_iter=-1, probability=True, random_state=None, shrinking=True,
                        tol=0.001, verbose=False)
        clf_3.fit(x_train, y_train)

        subjects_3_buffer.append(clf_3.score(x_test, y_test))
        subjects_3_time.append((time.time() - start_time))

        # Categorical variables classification
        y = labels
        start_time = time.time()

        x = categorical

        scaler_categorical = StandardScaler()
        x_transform = scaler_categorical.fit_transform(x)
        x_train, x_test, y_train, y_test = train_test_split(x_transform, y, test_size=0.20, stratify=y)

        clf_categorical = svm.SVC(C=c_selected, cache_size=200, class_weight='balanced', coef0=0.0,
                                  decision_function_shape='ovr', degree=3, gamma='auto', kernel=selected_kernel,
                                  max_iter=-1, probability=True, random_state=None, shrinking=True,
                                  tol=0.001, verbose=False)
        clf_categorical.fit(x_train, y_train)

        categorical_buffer.append(clf_categorical.score(x_test, y_test))
        categorical_time.append((time.time() - start_time))

        # Categorical variables with subjects of 1st course
        start_time = time.time()
        x = categorical_segment_1

        scaler_categorical_1 = StandardScaler()
        x_transform = scaler_categorical_1.fit_transform(x)
        x_train, x_test, y_train, y_test = train_test_split(x_transform, y, test_size=0.20, stratify=y)

        clf_categorical_1 = svm.SVC(C=c_selected, cache_size=200, class_weight='balanced', coef0=0.0,
                                    decision_function_shape='ovr', degree=3, gamma='auto',
                                    kernel=selected_kernel,
                                    max_iter=-1, probability=True, random_state=None, shrinking=True,
                                    tol=0.001, verbose=False)
        clf_categorical_1.fit(x_train, y_train)

        combined_1_buffer.append(clf_categorical_1.score(x_test, y_test))
        combined_1_time.append((time.time() - start_time))

        # Categorical variables with subjects of 2nd course
        start_time = time.time()
        x = categorical_segment_2

        scaler_categorical_2 = StandardScaler()
        x_transform = scaler_categorical_2.fit_transform(x)
        x_train, x_test, y_train, y_test = train_test_split(x_transform, y, test_size=0.20, stratify=y)

        clf_categorical_2 = svm.SVC(C=c_selected, cache_size=200, class_weight='balanced', coef0=0.0,
                                    decision_function_shape='ovr', degree=3, gamma='auto',
                                    kernel=selected_kernel,
                                    max_iter=-1, probability=True, random_state=None, shrinking=True,
                                    tol=0.001, verbose=False)
        clf_categorical_2.fit(x_train, y_train)

        combined_2_buffer.append(clf_categorical_2.score(x_test, y_test))
        combined_2_time.append((time.time() - start_time))

        # Categorical variables with subjects of 3rd course
        x = categorical_segment_3

        scaler_categorical_3 = StandardScaler()
        x_transform = scaler_categorical_3.fit_transform(x)
        x_train, x_test, y_train, y_test = train_test_split(x_transform, y, test_size=0.20, stratify=y)

        clf_categorical_3 = svm.SVC(C=1.0, cache_size=200, class_weight='balanced', coef0=0.0,
                                    decision_function_shape='ovr', degree=3, gamma='auto',
                                    kernel=selected_kernel,
                                    max_iter=-1, probability=True, random_state=None, shrinking=True,
                                    tol=0.001, verbose=False)
        clf_categorical_3.fit(x_train, y_train)

        combined_3_buffer.append(clf_categorical_3.score(x_test, y_test))

        combined_3_time.append((time.time() - start_time))

    destination_file.write("Subjects1o Mean Test set score: %f\n" % np.mean(subjects_1_buffer))
    destination_file.write("Subjects1o time Test set score: %f\n" % np.sum(subjects_1_time))
    destination_file.write("Subjects2o Mean Test set score: %f\n" % np.mean(subjects_2_buffer))
    destination_file.write("Subjects2o time Test set score: %f\n" % np.sum(subjects_2_time))
    destination_file.write("Subjects3o Mean Test set score: %f\n" % np.mean(subjects_3_buffer))
    destination_file.write("Subjects3o time Test set score: %f\n" % np.sum(subjects_3_time))
    destination_file.write("categorical Mean Test set score: %f\n" % np.mean(categorical_buffer))
    destination_file.write("categorical time Test set score: %f\n" % np.sum(categorical_time))
    destination_file.write("Combined1o Mean Test set score: %f\n" % np.mean(combined_1_buffer))
    destination_file.write("Combined1o time Test set score: %f\n" % np.sum(combined_1_time))
    destination_file.write("Combined2o Mean Test set score: %f\n" % np.mean(combined_2_buffer))
    destination_file.write("Combined2o time Test set score: %f\n" % np.sum(combined_2_time))
    destination_file.write("Combined3o Mean Test set score: %f\n" % np.mean(combined_3_buffer))
    destination_file.write("Combined3o time Test set score: %f\n\n" % np.sum(combined_3_time))

    print("CLASSIFIER REPORT\n")
    print("Subjects 1st course Mean Test set score: %f" % np.mean(subjects_1_buffer))
    print("Subjects 1st course time Test set score: %f\n" % np.sum(subjects_1_time))
    print("Subjects 1st + 2nd course Mean Test set score: %f" % np.mean(subjects_2_buffer))
    print("Subjects 1st + 2nd course time Test set score: %f\n" % np.sum(subjects_2_time))
    print("Subjects 1st + 2nd + 3rd course Mean Test set score: %f" % np.mean(subjects_3_buffer))
    print("Subjects 1st + 2nd + 3rd course time Test set score: %f\n" % np.sum(subjects_3_time))
    print("categorical Mean Test set score: %f" % np.mean(categorical_buffer))
    print("categorical time Test set score: %f\n" % np.sum(categorical_time))
    print("Subjects 1st course + categorical Mean Test set score: %f" % np.mean(combined_1_buffer))
    print("Subjects 1st course + categorical time Test set score: %f\n" % np.sum(combined_1_time))
    print("Subjects 1st + 2nd course + categorical Mean Test set score: %f" % np.mean(combined_2_buffer))
    print("Subjects 1st + 2nd course + categorical time Test set score: %f\n" % np.sum(combined_2_time))
    print("Subjects 1st + 2nd + 3rd course + categorical Mean Test set score: %f" % np.mean(combined_3_buffer))
    print("Subjects 1st + 2nd + 3rd course + categorical time Test set score: %f\n\n" % np.sum(combined_3_time))

    return
