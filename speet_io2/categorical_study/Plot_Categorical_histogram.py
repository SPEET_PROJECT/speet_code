# -*- coding: utf-8 -*-
"""
Created on Fri Feb 09 11:54:13 2018
@author: SPEET

Updated on Wed April 11 19:52:00 2018
@author: Alan
"""

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from matplotlib.backends.backend_pdf import PdfPages

from utils.utils import get_hex_color


def plot_categorical_histogram(categorical_list, path):
    """
    :param categorical_list: List of categories.
    :param path: Path where the files will be exported.
    """
    red = get_hex_color('red')
    blue = get_hex_color('blue')
    green = get_hex_color('green')

    for i in range(len(categorical_list)):
        destination_file = path + '/Categorical_Study/' + str(i) + '_performance_clusters_histogram.'
        df_categorical = pd.read_csv(path + '/Clusters/ScoresLabelsCategorical.csv', sep=',')
        df_categorical = df_categorical.round({'AdmissionScore': 0})

        pp = PdfPages(destination_file + 'pdf')

        d = {0: 'Average Students', 1: 'Excellent Students', 2: 'Low-Performance Students'}
        df_categorical['Label'].replace(d, inplace=True)

        colors = {'Average Students': red, 'Excellent Students': blue, 'Low-Performance Students': green}
        
        sns.countplot(x=categorical_list[i], hue="Label", palette=colors, data=df_categorical)
        plt.xticks(fontsize=8)
        red_patch = mpatches.Patch(color=red, label='Average Students')
        blue_patch = mpatches.Patch(color=blue, label='Excellent Students')
        green_patch = mpatches.Patch(color=green, label='Low-Performance Students')
        plt.legend(handles=[red_patch, blue_patch, green_patch], loc=1)
        plt.savefig(pp, format='pdf', transparent=True)
        plt.show()
        plt.close()
        pp.close()
    return
