# -*- coding: utf-8 -*-
"""
Created on Tue Nov 07 09:32:57 2017
@author: SPEET

Updated on Wed April 11 19:52:00 2018
@author: Alan
"""
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from numpy import linspace, meshgrid
from scipy.interpolate import griddata
import itertools

from . Plot_Categorical_histogram import plot_categorical_histogram
from utils.utils import get_hex_color


def plot_categorical_generator(data_frame, categorical, labels, path):
    """
    In this function we will plot all the the 2 combinations of the categorical

    :param data_frame: Data frame with all the marks, 2 PCA values, labels reordered and categorical variables
    :param categorical: List of lists with Categorical variables
    :param labels: Array with the ground truth clusters
    :param path: Path where the files will be exported.
    """
    data_frame.to_csv(path + '/Clusters/ScoresLabelsCategorical.csv', encoding='utf-8')

    categorical_list = ['PreviousStudies', 'Sex', 'Nationality', 'AccessToStudiesAge', 'AdmissionScore']
    sequence = '01234'

    combination_1 = list(itertools.combinations(categorical_list, 2))
    combination_2 = list(itertools.combinations(sequence, 2))
    plot_categorical_histogram(categorical_list, path)

    for i in range(len(combination_1)):
        tuple_1 = list(combination_1[i])
        categorical_1 = tuple_1[0]
        categorical_2 = tuple_1[1]
        tuple_2 = list(combination_2[i])
        index_1 = int(tuple_2[0])
        index_2 = int(tuple_2[1])
        plot_categorical(categorical_1, categorical_2, categorical, index_1, index_2, labels, i, path)

    return


def plot_categorical(categorical_1, categorical_2, categorical, index_1, index_2, labels, i, path):
    """
    :param categorical_1: First course.
    :param categorical_2: Two first courses.
    :param categorical: All courses.
    :param index_1: Index of first course.
    :param index_2: Two first courses indexes.
    :param labels: Array with the ground truth clusters
    :param i: Iterator of categorical_list combinations length
    :param path: Path where the files will be exported.
    """
    red = get_hex_color('red')
    blue = get_hex_color('blue')
    green = get_hex_color('green')

    i2 = str(i)
    destination_file = path + '/Categorical_Study/' + i2 + '_performance_clusters.pdf'
    gh = []
    for j in range(len(categorical)):
        gh.append(categorical[j][index_1])

    gh2 = []
    for j in range(len(categorical)):
        gh2.append(categorical[j][index_2])

    counts_1 = Counter([(x, y, z) for x, y, z in zip(gh, gh2, labels)])

    size = [counts_1[(x, y, z)] for x, y, z in zip(gh, gh2, labels)]  # To obtain the size of the dots
    test = np.column_stack((gh, gh2, size, labels))

    test = np.ascontiguousarray(test)  # This is for print the dots only one time
    unique_a = np.unique(test.view([('', test.dtype)] * test.shape[1]))
    out = unique_a.view(test.dtype).reshape((unique_a.shape[0], test.shape[1]))

    colormap = np.array([red, blue, green])

    if categorical_1 != 'PreviousStudies' and categorical_2 != 'PreviousStudies':
        pp = PdfPages(destination_file)
        plt.figure(figsize=(5, 5))
        color_int = out[:, 3].astype(int)
        size_scaled = out[:, 2]
        size_changed = 50 * np.array(size_scaled)
        size_int = size_changed.astype(int)
        plt.scatter(out[:, 0], out[:, 1], c=colormap[color_int], s=size_int, alpha=0.3)
        plt.title('Categorical comparision')
        plt.ylabel(categorical_2)
        plt.xlabel(categorical_1)
        plt.savefig(pp, format='pdf')
        plt.close()
        pp.close()

        data_frame_labels = pd.DataFrame(columns=['Labelstoorder'])
        data_frame_labels['Labelstoorder'] = color_int
        data_frame_labels['Labelstoorder'].replace([0, 1, 2], [1., 2., 0.], inplace=True)
        labels_reorder_1 = data_frame_labels.values
        labels_reorder_1 = labels_reorder_1.astype(float)
        np.savetxt(
            path + '/Categorical_Study/' + categorical_1 + categorical_2 + ".csv", out,
            delimiter=",")

    if categorical_1 == 'AccessToStudiesAge' and categorical_2 == 'AdmissionScore':
        res_x = 100
        res_y = 100

        xi = linspace(min(out[:, 0]), max(out[:, 0]), res_x)
        yi = linspace(min(out[:, 1]), max(out[:, 1]), res_y)
        hh = np.vstack((out[:, 0], out[:, 1]))
        hh = hh.T
        x, y = meshgrid(xi, yi)
        z = griddata(hh, labels_reorder_1[:, 0], (x, y), method='nearest')
        new_color_map = [green, red, blue]
        plt.contourf(x, y, z, levels=[0, 0.66, 1.9, 2], colors=new_color_map)
        plt.title('categorical contour')
        plt.ylabel(categorical_2)
        plt.xlabel(categorical_1)
        plt.savefig(path + '/Categorical_Study/' + categorical_1 + categorical_2 + 'contour.png')
        plt.close()

    return
