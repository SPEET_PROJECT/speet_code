# README #

GNU GPL v3 License.

UAB Algorithms for SPEET project.

STUDENTS’ DATA:
Due to confidentiality issues, data belonging to the different institutions are not provided. Instead, a toy example is considered.

Developers: Adrian Salvador Romero (UAB) and Alan Fuste Rodriguez (UAB)


### What is this repository for? ###

* Algorithms for Clustering and Classification


### How do I get set up? ###

* Python Version: 2.7.x or 3.6.x


### How do I run the code? ###

* Execute finalmain.py
* The plots, csv and txt results generated at folder '../exports/'


### Contribution guidelines ###

* data_preparation.py

This function will call to the respective data_preparation_institution.
The goal of this functions is read the .xls with the scores information and build the data frames

Imports needed:
pandas,
numpy


* Clustering.py

Using all the segmentation variables apply PCA obtaining a two dimensions array,then apply a Kmeans(n_clusters = 3) apply ELBOW analysis to segmentation variables (in this case the scores) in order to fix the number of clusters and print the results on the screen.

Imports needed:
sklearn,
csv


* visualization.py 

In this function we print visualization results associated to students clusters.

Imports needed:
matplotlib,
numpy


* label_reorder.py

In this function we assign always the same labels to clusters: 
    Excellent students, 1, Blue (#007BFF)
    Average Students, 0, Red (#DC3545)
    Low-performance Students, 2, Green (#28A745)

Imports needed:
numpy


* data_average_histograms.py

This function compute and plot the average score subjects and the average score students

Imports needed:
matplotlib


* data_preparation2.py

Second processing, we add categorical variables. In this function we will read the students information in order to 
characterize the clusters


* Plot_Categorical.py

In this function we will plot all the combinations of the categorical variables.

Imports needed:
itertools,
scipy,
collections
pandas,
numpy,
matplotlib


* Train_Predict_SVM_unbalanced.py

In this function we will scale and train the seven SVM classifiers: 1st course scores, 2nd course scores, 3rd course 
scores, categorical variables, 1st scores + categorical, 2nd scores + categorical and 3rd scores + categorical.

Imports needed:
pandas,
sklearn,
time


* Imports needed resume:
pandas,
numpy,
sklearn,
csv,
matplotlib,
itertools,
scipy,
collections,
time


### Who do I talk to? ###

* Jose Lopez Vicario (UAB) jose.vicario@uab.cat
 