# -*- coding: utf-8 -*-
"""
Created on Fri May 4 15:10:45 2018

@author: Alan
"""


def apply_preparation(categorical):
    different_previous_studies = categorical['PreviousStudies'].unique()
    replace_dict_aux = {}
    replace_dict = {}
    if len(different_previous_studies) == 1:
        replace_dict_aux = {different_previous_studies[0]: 1}
    elif len(different_previous_studies) == 2 or len(different_previous_studies) == 3:
        previous_studies_index = categorical['PreviousStudies'].value_counts().index
        for i in range(len(different_previous_studies)):
            if i >= 1:
                replace_dict_aux[previous_studies_index[i]] = 0
            else:
                replace_dict_aux[previous_studies_index[i]] = 1
    else:
        possible_values = [[0, 0], [0, 1], [1, 0]]
        previous_studies_index = categorical['PreviousStudies'].value_counts().index
        categorical.insert(loc=categorical.columns.get_loc('PreviousStudies') + 1, column='PreviousStudies2',
                           value=categorical['PreviousStudies'])
        replace_dict_aux2 = {}
        for i in range(len(different_previous_studies)):
            if i >= 3:
                replace_dict_aux[previous_studies_index[i]] = 1
                replace_dict_aux2[previous_studies_index[i]] = 1
            else:
                replace_dict_aux[previous_studies_index[i]] = possible_values[i][0]
                replace_dict_aux2[previous_studies_index[i]] = possible_values[i][1]
        replace_dict['PreviousStudies2'] = replace_dict_aux2
    replace_dict['PreviousStudies'] = replace_dict_aux

    for column in ['Sex', 'Nationality']:
        column_index = categorical[column].value_counts().index
        different_column_values = categorical[column].unique()
        replace_dict_aux = {}
        for i in range(len(different_column_values)):
            if i >= 1:
                replace_dict_aux[column_index[i]] = 0
            else:
                replace_dict_aux[column_index[i]] = 1
        replace_dict[column] = replace_dict_aux

    categorical.replace(to_replace=replace_dict, value=None, inplace=True)

    return categorical.values.tolist()


def data_preparation2(data_frame_init, subjects_1, subjects_12):
    """
    The goal of this functions is read the .xls with the scores information and build the data_processing frames.

    :param data_frame_init: Data frame with all the marks, 2 PCA values, labels reordered and categorical variables
    :param subjects_1: Subjects of the first course
    :param subjects_12: Subjects of first and second courses

    :return categorical: List of lists with Categorical variables
    :return categorical_for_histograms: List of lists with Categorical variables (always 2 bits in Previous Studies)
    :return categorical_1: List of lists with marks of the 1st course and Categorical variables
    :return categorical_12: List of lists with marks of the 2nd course and Categorical variables
    :return categorical_123: List of lists with marks of the 3rd course and Categorical variables
    """
    categorical_list = ['PreviousStudies', 'Sex', 'Nationality', 'AccessToStudiesAge', 'AdmissionScore']

    data_frame = data_frame_init.copy(deep=True)
    categorical = apply_preparation(data_frame[categorical_list])

    categorical_list_1 = subjects_1.astype(str).tolist() + categorical_list
    data_frame = data_frame_init.copy(deep=True)
    categorical_1 = apply_preparation(data_frame[categorical_list_1])

    categorical_list_12 = subjects_12.astype(str).tolist() + categorical_list
    data_frame = data_frame_init.copy(deep=True)
    categorical_12 = apply_preparation(data_frame[categorical_list_12])

    data_frame = data_frame_init.copy(deep=True)
    categorical_123 = apply_preparation(data_frame)

    return categorical, categorical_1, categorical_12, categorical_123, data_frame
