# -*- coding: utf-8 -*-
"""
Created on Tue May 8 16:52:00 2018
@author: Alan
"""
import pandas as pd
import numpy as np


def data_preparation(path, degree):
    """
    :param path: Directory.
    :param degree: Degree.

    csv1: Performance.
    csv2: Students.
    csv3: Degree.
    """
    # STUDENTS
    df_students = pd.read_csv(path + '/data/students.csv', sep=';', error_bad_lines=False)
    df_students.dropna(axis=0, inplace=True)
    df_students = df_students.astype('str')

    df_students = df_students[df_students['DegreeID'] == degree]

    df_students = df_students[(df_students['Status'] == 'G')]
    df_students.drop('Status', axis=1, inplace=True)

    df_students.loc[:, 'AccessToStudiesAge'] = pd.to_numeric(df_students.loc[:, 'AccessToStudiesAge'], errors='coerce',
                                                             downcast='integer')

    df_students.sort_values(by='AccessToStudiesAge', inplace=True)
    df_students.drop_duplicates(subset=['StudentID'], keep='last', inplace=True)

    df_students.set_index('StudentID', inplace=True)

    df_students.loc[:, 'AdmissionScore'] = pd.to_numeric(df_students.loc[:, 'AdmissionScore'], errors='coerce',
                                                         downcast='float').round(2)

    # SUBJECTS PERFORMANCE
    df_performance = pd.read_csv(path + '/data/subjects_performance.csv', sep=';', error_bad_lines=False)
    df_performance = df_performance.astype('str')

    df_performance = df_performance[df_performance['DegreeID'] == degree]

    df_performance = df_performance[df_performance['SubjectNature'] == 'Mandatory']

    df_performance.loc[:, 'SubjectYear'] = pd.to_numeric(df_performance.loc[:, 'SubjectYear'], errors='coerce',
                                                         downcast='integer')

    df_performance.loc[:, 'SubjectNumberECTS'] = pd.to_numeric(df_performance.loc[:, 'SubjectNumberECTS'],
                                                               errors='coerce', downcast='integer')
    df_performance = df_performance[df_performance['SubjectNumberECTS'] >= 3]

    df_performance.loc[:, 'SubjectScore'] = pd.to_numeric(df_performance.loc[:, 'SubjectScore'], errors='coerce',
                                                          downcast='float')
    df_performance['SubjectScore'].fillna(5.00, inplace=True)
    df_performance.loc[:, 'SubjectScore'] = df_performance.loc[:, 'SubjectScore'].round(2)
    df_performance.sort_values(by='SubjectScore', inplace=True)
    df_performance.drop_duplicates(subset=['StudentID', 'SubjectName'], keep='last', inplace=True)

    df_performance = df_performance[df_performance['StudentID'].isin(df_students.index)]

    subjects = df_performance[
        ['SubjectID', 'SubjectName', 'SubjectNumberECTS', 'SubjectYear', 'SubjectSemester', 'SubjectNature']]

    df_performance = df_performance.pivot(index='StudentID', columns='SubjectName', values='SubjectScore')

    # Check subject NA (required more than 50% of valid scores) and after deleting columns we fill NA with 5.0
    if df_performance.isnull().values.any():
        required_valid_data = int(len(df_performance.index) * 0.5 + 1)
        df_performance.dropna(axis=1, how='all', thresh=required_valid_data, inplace=True)
        df_performance.fillna(5.00, inplace=True)

    data_frame = df_performance.join(df_students)

    # Pre process data
    valid_subjects = data_frame.drop(
        ['Sex', 'AccessToStudiesAge', 'Nationality', 'PreviousStudies', 'AdmissionScore', 'DegreeID'],
        axis=1).columns.values.tolist()
    subjects = subjects[subjects['SubjectName'].isin(valid_subjects)]

    subjects.drop_duplicates(subset=['SubjectName'], keep='last', inplace=True)
    subjects.set_index('SubjectName', inplace=True)

    subjects_1 = np.array(subjects[subjects['SubjectYear'].astype(int) == 1].index.values.tolist()).astype(str).tolist()
    subjects_12 = np.array(subjects[subjects['SubjectYear'].astype(int) <= 2].index.values.tolist()).astype(
        str).tolist()
    subjects_123 = np.array(subjects[subjects['SubjectYear'].astype(int) <= 3].index.values.tolist()).astype(
        str).tolist()

    df_marks = data_frame.drop(['Sex', 'AccessToStudiesAge', 'Nationality', 'PreviousStudies', 'AdmissionScore',
                                'DegreeID'], axis=1)
    df_marks.columns = df_marks.columns.astype(str)

    df_marks_subjects = np.array(df_marks.columns.tolist()).astype(str)

    subjects_1 = np.intersect1d(subjects_1, df_marks_subjects)
    subjects_12 = np.intersect1d(subjects_12, df_marks_subjects)
    subjects_123 = np.intersect1d(subjects_123, df_marks_subjects)

    marks_1 = df_marks[subjects_1].values
    marks_12 = df_marks[subjects_12].values
    marks_123 = df_marks[subjects_123].values

    students = data_frame.index

    return data_frame, marks_123, subjects_123, students, df_marks[subjects_1], subjects_1, marks_1, df_marks[subjects_12], marks_12, df_marks[subjects_123], marks_123, subjects_12
