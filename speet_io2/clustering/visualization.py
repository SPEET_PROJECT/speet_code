# -*- coding: utf-8 -*-
"""
Created on Mon Sep 04 22:23:34 2017
@author: Adrian

Updated on Wed April 11 19:52:00 2018
@author: Alan
"""
import numpy as np
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

from utils.utils import get_hex_color


def visualization(transform, labels, path):
    """
    In this function we will visualize the data and clusters in 2D and 3D

    :param transform: 2D array with the result of apply PCA(n_components=2) to segmentation_variables
    :param labels: Array with the ground truth clusters
    :param path: Route where the files will be exported
    """
    red = get_hex_color('red')
    blue = get_hex_color('blue')
    green = get_hex_color('green')

    plt.figure(figsize=(5, 5))
    colormap = np.array([red, blue, green])
    plt.scatter(transform[:, 0], transform[:, 1], c=colormap[labels], s=30)

    plt.title('Performance clusters')
    plt.ylabel('PC2')
    plt.xlabel('PC1')
    red_patch = mpatches.Patch(color=red, label='Average Students')
    blue_patch = mpatches.Patch(color=blue, label='Excellent Students')
    green_patch = mpatches.Patch(color=green, label='Low-Performance Students')
    plt.legend(handles=[red_patch, blue_patch, green_patch], loc=1)
    plt.tight_layout()
    plt.show()

    pp = PdfPages(path + '/Clusters/performance_clusters.pdf')
    plt.savefig(pp, format='pdf', transparent=True)
    plt.close()
    pp.close()

    return
