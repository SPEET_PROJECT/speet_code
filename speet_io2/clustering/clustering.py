# -*- coding: utf-8 -*-
"""
Created on Thu Sep 07 12:20:54 2017
@author: SPEET

Updated on Wed April 11 19:52:00 2018
@author: Alan
"""
import csv
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from sklearn.decomposition import PCA
import pandas as pd

from label_reorder.label_reorder import label_reorder


def clustering(segmentation_variables, data_frame, subjects, path):
    """
    Using all the segmentation variables apply PCA obtaining a 2 dimensions array,
    then aply a Kmeans(n_clusters = 3) apply ELBOW analysis to segmentation variables
    (in this case the scores) in order to fix the number of clusters and
    print the views on the screen

    :param data_frame: Data frame with all the marks
    :param subjects: List with all the subject names
    :param segmentation_variables: 2D array with all the marks per student
    :param path: Path where the files will be exported.

    :return data_frame: Data frame with all the marks, 2 PCA values, labels reordered and categorical variables
    :return transform: 2D array with the result of apply PCA(n_components=2) to segmentation_variables
    :return labels: Array with the ground truth clusters
    """

    file_name = open(path + '/Clusters/Silhouette.csv', 'w')
    wr = csv.writer(file_name)

    #   Elbow analysis
    for i in range(2, 11):
        kmeans = KMeans(n_clusters=i).fit(segmentation_variables)
        label = kmeans.labels_
        silhouette_coefficient = silhouette_score(segmentation_variables, label, metric='euclidean')

        print("For {} clusters, The Silhouette Coefficient is {}".format(i, silhouette_coefficient))

        wr.writerow(str(i) + '    ' + str(silhouette_coefficient))

    pca = PCA(n_components=2)
    transform = pca.fit_transform(segmentation_variables)

    kmeans = KMeans(n_clusters=3, random_state=0).fit(transform)
    labels = kmeans.labels_

    data_frame['Label'] = pd.Series(labels, index=data_frame.index)
    data_frame['feature0'] = pd.Series(transform[:, 0], index=data_frame.index)
    data_frame['feature1'] = pd.Series(transform[:, 1], index=data_frame.index)

    data_frame, labels = label_reorder(data_frame, subjects)

    return data_frame, transform, labels
