﻿IO3 - README
-----------------------------------------------

GNU GPL v3 License.

Visualization tools developed for the Intellectual Output #3 of the SPEET Project, developed by the University of Leon.

Organization of the contribution
-------------------------------------
The contribution is organized as follows:

+ 'app/': This structure of directories contains the visualization tools, implemented in JavaScript/Processing, as well as the libraries,
		 web resources, web servers and data sets required by them to work properly
		 
	- 'app/static/lib/': It includes the developed visualization tools ('cvDataCube.js', 'dr_degree.js', and 'dr_year.js'), the required libraries 
						(D3, Crossfilter, DC.js, Bootstrap, jQuery and Processing.js)
	- 'app/static/data/dr1_data/': Data for the Dimensionality Reduction (by Year)
	- 'app/static/data/dr2_data/': Data for the Dimensionality Reduction (by Degree)
	- 'app/static/data/histograms': Data for the Coordinated Views
	- 'app/templates/': Web pages where the tools are integrated.
	- 'app/routes.py': Web server. It has to be run so that the tools are available to a user.
			  
Running the code
--------------------------------------
Inside 'app/', run 'python routes.py' from the console. As a result, the visualization tools would be available locally from a web browser, generally in the address
'http://127.0.0.1:5000/'. The visualization tools should render and work properly in modern browsers such as the latest versions of Mozilla Firefox and Google Chrome,
provided that the screen resolution is at least 1280x800.

Prerequisites: Python 3 and Flask. Flask might be installed using the command 'pip install Flask' in common Python environments. For further information, 
			http://flask.pocoo.org/docs/0.12/installation/
			   
