from flask import Flask, render_template, send_file

app=Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/coordinated_views')
def coordinated_views():
    return render_template('coordinated_views.html')


@app.route('/dimension_reduction1')
def dimension_reduction1():
    return render_template('dimension_reduction1.html')


@app.route('/dimension_reduction2')
def dimension_reduction2():
    return render_template('dimension_reduction2.html')


@app.route('/data_cv')
def data_cv():
    return send_file('static/data/histograms/data_cv.csv', mimetype='text/csv', attachment_filename='data_cv.csv',
                     as_attachment=True)


@app.route('/nuts2')
def nuts2():
    return send_file('static/data/histograms/nuts2.json', mimetype='text/json', attachment_filename='nuts2.json')


@app.route('/dr_year/<file>')
def dr_year(file):
    return send_file('static/data/dr1_data/' + file + '.csv',  mimetype='text/csv', attachment_filename=file+'.csv')


@app.route('/dr_degree/<file>')
def dr_degree(file):
    return send_file('static/data/dr2_data/' + file + '.csv',  mimetype='text/csv', attachment_filename=file+'.csv')


if __name__ == '__main__':
    app.run(debug=True)

