/*
*
* Copyright (C) 2018 Antonio Morán, Universidad de León
* This file (dr_degree) is part of the SPEET (Student Profile for Enhancing Engineering Tutoring) 
* Project, financed by the European Commission in the framework of the Erasmus+ programme.
*
* dr_degree is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
*
* dr_degree is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License.

* The European Commission support for the production of this publication does not constitute 
* an endorsement of the contents which reflects the views only of the authors, and the 

* Commission cannot be held responsible for any use which may be made of the information contained therein
*
*/

//Variable declaration

// Variables related with the HTML
var vList1 = document.getElementById("list1");
var vList2 = document.getElementById("list2");
var vList3 = document.getElementById("list3");
var selec = 0; 
int newData = 1;
int degree = 0;
int list1 = 0;
int list2 = 0;
int list3 = 0;

// Original variables
String[][] variables;

// Unique values
String[][] cases;

// Data and radial basis functions
float[][] data;
int datasel=0;
float[][] grnn;

// Coordinates
float[][] coord1;
float[][] coord;
float[][] cogrnn;
float[][] coordline;

// Maximum and minimum values
float[][] maxmin;

// Color map
float[][] colormap;


// Constants
final int WIDTH=1280;
final int HEIGHT=860;
final int ANGRA=275;
final int MINSIZE=10;
final int MAXSIZE=32;
final int FRAME_RATE=30;
final float LAMBDA=0.2;
final int BACKGROUND = 200;
final int TEXT_WIDTH = 600;
final int TEXT_HEIGHT = 50;
final int LEGEND_WIDTH = 200;
final int LEGEND_HEIGHT = 500;
final int TITLE_SIZE = 30;
final int TRANSPARENCY_HIGH = 220;
final int TRANSPARENCY_LOW = 10;
final int STROKE1 = 250;
final int STROKE2 = 255;
final int STROKE3 = 100;
final int STROKE4 = 50;
final int TRANSLATION1 = 150;
final int TRANSLATION2 = 175;
final int DISTANCE1 = 100;
final int COLOR1 = 100;
final int COLOR2 = 50;
final int COLOR3 = 255;
final int COLOR4 = 150;
final int RECT1A = 95;
final int RECT1B = 0;
final int RECT1C = 125;
final int RECT1D = 20;
final int RECT2A = 20;
final int RECT2B = 0;
final int RECT2C = 110;
final int RECT2D = 20;
final int RECT3A = 145;
final int RECT3B = 0;
final int RECT3C = 110;
final int RECT3D = 20;
final int RECT4A = 50;
final int RECT4B = -50;
final int RECT4C = 100;
final int RECT4D = 25;
final int TEXT_SIZE1 = 14;
final int TEXT_SIZE2 = 11;
final int TEXT_LENGTH = 18;


// User input
boolean mousep=false;
boolean mousep2=false;
boolean mousepa=false;
boolean fixpoint=false;
boolean showline=false;
boolean draw_data=false;



// Definición de variables
int[] binaries = {0, 6, 10, 11, 12, 14};
//int[] binaries = {0, 6, 10, 11, 12, 14};
int[] analogs = {1, 3, 4, 5, 7, 8, 9, 13};
float[] v_graf = new float[21];
float[] v_graf2 = new float[21];


// Variables para el control de la animación de los dibujos
float[] lonbar1 = new float[analogs.length];
float[] lonbar12 = new float[analogs.length];


boolean[] varsel = new boolean[21];
float[] varval = new float[21];




/*
* Initialization
*/
void setup() {
 
  size(WIDTH,HEIGHT);
  frameRate(FRAME_RATE);

  // Loading values
  variables = loadcsv("./dr_degree/Variables");
  data = loadcsvfloat("./dr_degree/Data");
  coord1 = loadcsvfloat("./dr_degree/Coordinates");
  maxmin = loadcsvfloat("./dr_degree/Maxmin");
  colormap  = loadcsvfloat("./dr_degree/Colormap");
  cases = loadcsv("./dr_degree/Cases");

  listselection();
  coord = rescale(coord1);
}

/*
* Runs continously
*/
void draw() {
  float distance=-1;
  float dist_ant=DISTANCE1;

  int numsel=0;
  int[] numselarr= new int[numsel];
  int s=0;
	
  background(BACKGROUND);
  textAlign(CENTER, BOTTOM);
  fill(0);
  textSize(TITLE_SIZE);
  text("SPEET: Projection by degree", TEXT_WIDTH, TEXT_HEIGHT);
  
  // Values are obtained from the HTML elements
  newData = 0; 
  selec.value;
  list1 = vList1.value;
  list2 = vList2.value;
  degree = vList3.value

  // Lines are drawn
  if (fixpoint & showline){
    pushMatrix();
    drawLine();
    popMatrix();
  }
    
  // Points are drawn 
  for (int i=0; i<varsel.length; i++){
    if (varsel[i]){numsel=numsel+1;}
  }
  
  
  for (int i=0; i<varsel.length; i++){
    if (varsel[i]){numselarr[s]=i; s=s+1;}
  }
  
  if (fixpoint==false){
    for (int i=0; i<maxmin.length; i++){
      v_graf[i]=maxmin[i][1];
      v_graf2[i]=maxmin[i][1];
    }
    draw_data=false;
  }
  
  for(int i=0;i<coord.length-(5-newData); i++){

    boolean plotval=true;
    int transparency=TRANSPARENCY_HIGH;
    
    if (numselarr.length>0 && i!=datasel){
      for(int j=0; j<numselarr.length; j++){
        if (data[i][numselarr[j]]!=varval[numselarr[j]]){plotval=false; transparency=TRANSPARENCY_LOW;}
      }
    }     
    
    stroke(STROKE1, transparency);
    if (i>=coord.length-5){
      stroke(STROKE2,0,0, transparency);
    }
    

    if (data[i][14]==degree){

	    int ind=int(data[i][2])*25;
	    fill(int(colormap[ind][0]), int(colormap[ind][1]), int(colormap[ind][2]),transparency);
	        
	    int shapeCase=int(data[i][binaries[int(list2)]]);
	    int tam = int(map(data[i][analogs[int(list1)]], maxmin[analogs[int(list1)]][1], maxmin[analogs[int(list1)]][0], MINSIZE, MAXSIZE));

	    if(i==datasel){
	      stroke(0,0,STROKE2);
	    }
	    switch(shapeCase){
	     case 0: ellipse(coord[i][0]+tam/2,coord[i][1]+tam/2,tam,tam); break;
	     case 1: rect(coord[i][0],coord[i][1],tam,tam); break;
	     case 2: triangle(coord[i][0]+(tam/2),coord[i][1], coord[i][0], coord[i][1]+tam, coord[i][0]+tam, coord[i][1]+tam); break;
	     case 3: rect(coord[i][0],coord[i][1],tam,tam,5); break;
	     case 4: polygon(coord[i][0],coord[i][1],tam,5); break;
	     default: ellipse(coord[i][0]+tam/2,coord[i][1]+tam/2, tam, tam); break;
	   }
	
   
	   if (plotval){
	      distance = dist(coord[i][0]+(tam/2), coord[i][1]+(tam/2), mouseX, mouseY);
	      if ((distance<tam/2)&(distance<dist_ant)){
	         dist_ant=distance;
           draw_data=true;
	         for (int j=0; j<data[i].length; j++){
	           if (fixpoint){
	             v_graf2[j] = data[i][j];
	           }else{
	             datasel = i;
	             v_graf[j] = data[i][j];
	          }
	        }  
	        if (mousePressed){
	          fixpoint=true;
	        }     
	      }
	    }
    }    
  }
  
  noStroke();
  
  // Legend is drawn
  pushMatrix();
  translate(0, TRANSLATION1);
  legend(LEGEND_WIDTH, LEGEND_HEIGHT);
  popMatrix();  
  
  
  // Charts are drawn
  pushMatrix();
  translate(WIDTH-ANGRA,0);
  charts(ANGRA, v_graf, v_graf2, draw_data);  
  popMatrix();
  
}


/*
* Reads CSV float data
* @param String path
* @return float[][]   data
*/
float[][] loadcsvfloat(String archivo) 
{
  String lines[] = loadStrings(archivo);
  float[][] csv;
  int csvWidth=0;

  // Calculate max width of csv file
  for (int i=0; i < lines.length; i++) {
    String [] chars=split(lines[i], ',');
    if (chars.length>csvWidth) {
      csvWidth=chars.length;
    }
  }

  // Create csv array based on # of rows and columns in csv file
  csv = new float [lines.length][csvWidth];

  // Parse values into 2d array
  for (int i=0; i < lines.length; i++) {
    String [] temp = new String [lines.length];
    temp= split(lines[i], ',');
    for (int j=0; j < temp.length; j++) {
      csv[i][j]=float(temp[j]);
    }
  }
  return csv;
}


/*
* Reads CSV text data
* @param String path
* @return String[][]   data
*/
String[][] loadcsv(String archivo) 
{
  String lines[] = loadStrings(archivo);
  String[][] csv;
  int csvWidth=0;

  //calculate max width of csv file
  for (int i=0; i < lines.length; i++) {
    String [] chars=split(lines[i], ',');
    if (chars.length>csvWidth) {
      csvWidth=chars.length;
    }
  }

  //create csv array based on # of rows and columns in csv file
  csv = new String [lines.length][csvWidth];

  //parse values into 2d array
  for (int i=0; i < lines.length; i++) {
    String [] temp = new String [lines.length];
    temp= split(lines[i], ',');
    for (int j=0; j < temp.length; j++) {
      csv[i][j]=temp[j];
    }
  }
  return csv;
}

/*
* Rescales data
* @param float[][] coord1   Original data
* @return float[][]    Rescaled data 
*/
float[][] rescale(float[][] coord1){
  coord = new float[coord1.length][2];
  for (int i=0; i<coord1.length; i++){
    for (int j=0; j<coord1[i].length; j++){
      if ((j%2)==1){
        coord1[i][j]=map(coord1[i][j],-75,50,0,800);
        coord[i][1]=coord1[i][1];
      }
      else{
        coord1[i][j]=map(coord1[i][j],-75,45,0,1000);
        coord[i][0]=coord1[i][0];
      }
    }
  }
  return coord;
}

/*
* Draws rectangle
* @param int posx
* @param int posy
* @param int xw
* @param int yw
* @param int rIndex
*/
void drawrect(int posx, int posy, int xw, int yw, int rIndex){
  
  fill(0);
  if (varsel[rIndex]){ 
    fill(COLOR1); 
  }
  
  int posratx = int(mouseX-screenX(posx, posy));
  int posraty = int(mouseY-screenY(posx, posy));
   
  if ((posratx>=0)&(posraty>=0)&(posratx<=xw)&(posraty<=yw)){
    fill(COLOR2);
    if (rIndex == -1){
      if(mousep){
        fixpoint=false;
      }
    }else{
      if (mousep){
        varsel[rIndex]=(varsel[rIndex]&false)|(!varsel[rIndex]&true);
        varval[rIndex]=v_graf[rIndex];
        mousep=false;
      }
      else if(mousep2){
        varsel[rIndex]=(varsel[rIndex]&false)|(!varsel[rIndex]&true);
          if(rIndex==7 || rIndex==14 || rIndex==15){
            varval[rIndex]=v_graf[rIndex]+1;
          }else{
            if (v_graf[rIndex]==1){
              varval[rIndex]=0;
            }else{
              varval[rIndex]=1;
            }
          }

        v_graf[rIndex]=varval[rIndex];
        mousep2=false;

        
      }
    }
  }
  rect(posx, posy, xw, yw);
}


/*
* Mouse pressed
*/
void mousePressed(){
  if (mouseButton == LEFT) {
      mousep=true;
  }else if (mouseButton == RIGHT) {
      mousep2=true;
  }  
}
     
   
/*
* List selection
*/   
void listselection(){

  for(int i=0; i<analogs.length; i++) {
    var el = document.createElement("option");
    el.textContent = variables[analogs[i]];
    el.value = i;
    vList1.appendChild(el);
  }

  for(int i=1; i<(binaries.length)-1; i++) {
    var el = document.createElement("option");
    el.textContent = variables[binaries[i]];
    el.value = i;
    vList2.appendChild(el);
  }

  for(int i=0; i<33; i++) {
    var el = document.createElement("option");
    el.textContent = cases[6][i];
    el.value = i;
    vList3.appendChild(el);
  }
}


/*
* Draws right-side charts of the original variables
* @param int x
* @param float[] valg
* @param float[] valg2
*/
void charts(int x, float[] valg, float[] valg2, boolean draw_d){
 
 int[] y = {250, 200, 200};
  
 fill(COLOR3);
 translate(0,TRANSLATION2); 
 strokeWeight(2);
 stroke(0);
 rect(0,0,x,y[0]);
 rect(0,y[0],x,y[1]);
 
 // Draws upper square
 textSize(TEXT_SIZE1);
 fill(0);
 textAlign(CENTER, BOTTOM);
 text("Student Data", x/2, 20);
 textAlign(LEFT, CENTER);
 
 pushMatrix();
 noStroke(); 
 for(int i=0; i<analogs.length; i++){
   textSize(TEXT_SIZE2);
   translate(0, 25);
   text(variables[analogs[i]][0], 5, 10);
   fill(COLOR4);
   rect(RECT1A, RECT1B, RECT1C, RECT1D);
     
     fill(COLOR3, 0, 0); 
     lonbar1[i] =lonbar1[i]+ LAMBDA*round(map(valg[analogs[i]], maxmin[analogs[i]][1], maxmin[analogs[i]][0], 0, 110)-lonbar1[i]) ;
     rect(95, 3, lonbar1[i], 15);
     fill(0);
     text(valg[analogs[i]], 225, 10); 
   
   if (fixpoint){
     fill(0, 0, COLOR3, COLOR1); 
     lonbar12[i] =lonbar12[i]+ LAMBDA*round(map(valg2[analogs[i]], maxmin[analogs[i]][1], maxmin[analogs[i]][0], 0, 110)-lonbar12[i]) ;
     rect(95, 4, lonbar12[i], 13);
     fill(0, 0, COLOR3);
     textSize(8);
     text(valg2[analogs[i]], 225, 20); 
     fill(0);
   }
 }  
    
 popMatrix();
 
 // Draws medium square
 translate(0,y[0]);
 textSize(14);
 fill(0);
 textAlign(CENTER, BOTTOM);
 text("Student Parameters", x/2, 20);
 
 pushMatrix();
 translate(0,5);
 textSize(10);
 
 for(int i=0; i<(binaries.length)/2; i++){
   fill(0);
   textAlign(LEFT, CENTER);
   textSize(TEXT_SIZE2);
   translate(0,35);
   text(variables[binaries[2*i]][0], 20, 0);
   text(variables[binaries[2*i+1]][0],145, 0);
   
   textAlign(CENTER, CENTER);
   translate(0, 10);
   fill(0); 
   drawrect(RECT2A,RECT2B,RECT2C,RECT2D,binaries[2*i]);
   fill(COLOR3);

   if (varsel[binaries[2*i]]==true){
     text(String(cases[2*i][int(varval[binaries[2*i]])]).substring(0,TEXT_LENGTH),int(x/4),10);
   }else{
    if(draw_d){
     text(String(cases[2*i][int(valg[binaries[2*i]])]).substring(0,TEXT_LENGTH),int(x/4),10);
    }
   }
  
   if(fixpoint){
     fill(0,0,COLOR3);
     textSize(8);
     text(String(cases[2*i][int(valg2[binaries[2*i]])]).substring(0,TEXT_LENGTH),int(x/4),24);
     text(String(cases[2*i+1][int(valg2[binaries[2*i+1]])]).substring(0,TEXT_LENGTH),int(3*x/4),24);
     textSize(TEXT_SIZE2);
     fill(0);
   }
   
   fill(0); 
   drawrect(RECT3A,RECT3B,RECT3C,RECT3D,binaries[2*i+1]);
   fill(COLOR3);
   if (varsel[binaries[2*i+1]]==true){
     text(String(cases[2*i+1][int(varval[binaries[2*i+1]])]).substring(0,TEXT_LENGTH),int(3*x/4),10);
   }else{
    if(draw_d){
     text(String(cases[2*i+1][int(valg[binaries[2*i+1]])]).substring(0,TEXT_LENGTH),int(3*x/4),10);
    }
   }
   fill(0);
}
 popMatrix();
}


/*
* Draws legend
* @param int x
* @param int y
*/
void legend(int x, int y){
 
 fill(COLOR4, COLOR2);
 rect(0,0,x,y);
  
 // Reset button
 drawrect(RECT4A, RECT4B, RECT4C, RECT4D, -1);
 textSize(12);
 fill(COLOR3);
 text('Reset', 100, -30);

 // Legend: radius
 textSize(TEXT_SIZE1);
 fill(0);
 textAlign(CENTER, BOTTOM);
 text("Radius", x/2, 30);
 textAlign(LEFT, CENTER);
 
 pushMatrix();
 textSize(10);
 translate(20,20);
 for(int i=0;i<4;i++){
   translate(0,25);
   fill(COLOR1);
   ellipse(10,0,(MAXSIZE*(1-float(i)/4)),(MAXSIZE*(1-float(i)/4)));
   fill(0);
   float valorrad = int(((maxmin[analogs[int(list1)]][0] - maxmin[analogs[int(list1)]][1])*(4-i)/4)+maxmin[analogs[int(list1)]][1]);
   text(valorrad, 25,0); 
 }
 popMatrix();

 translate(0, int(y/3));
 stroke(100, 50);
 line(0,0,x,0);
 noStroke();
 
 // Legend: shape
 textSize(TEXT_SIZE1);
 fill(0);
 textAlign(CENTER, BOTTOM);
 text("Shape  ", x/2, 25);
 textAlign(LEFT, CENTER);

 pushMatrix();
 textSize(TEXT_SIZE2);
 translate(20,10);
 for(int i=0; i<=int(maxmin[binaries[int(list2)]][0]); i++){
	 /////////
	 if (i>4){
		 break;
	 }
   translate(0,25);
   fill(COLOR1);
   switch(i){
     case 0: ellipse(15/2,15/2,15,15); break;
     case 1: rect(0,0,15,15); break;
     case 2: triangle(int(15/2),0, 0, 15, 15, 15); break;
     case 3: rect(0,0,15,15,5); fill(0); break;
     case 4: polygon(8,5,8,5); break;
   }
   fill(0);
   text(cases[int(list2)][i], 20,5);
 }
 popMatrix();
  
 translate(0, int(y/3));
 stroke(STROKE3, STROKE4);
 line(0,0,x,0);
 noStroke();
 
 // Legend: color
 textSize(TEXT_SIZE1);
 fill(0);
 textAlign(CENTER, BOTTOM);
 text("Color (Score)", x/2, 25);
 textAlign(LEFT, CENTER);

 pushMatrix();
 translate(25, 30);
 textSize(10);
 int alt=120;
 int altrec=2;
 int nrect = int(alt/altrec);
 for (int i=0; i<nrect; i++){
  fill(colormap[int(256/nrect)*i][0],colormap[int(256/nrect)*i][1],colormap[int(256/nrect)*i][2]);
  rect(0,0,15,altrec);
  translate(0,altrec);
 }
 popMatrix();

 pushMatrix();
 translate(45,30);
 fill(0);
 textSize(10);
 for (int i=0; i<11; i=i+2){
  text(str(i), 0, 0);
  translate(0, alt/5);
 }
 popMatrix(); 
}

/*
* Draws line
*/
void drawLine(){
  
  float cx=0;
  float cy=0;
  float[][] coordlinepol = new float [coordline.length][coordline[0].length];

  for (int i=0; i< coordline.length; i++){
    cx = cx + coordline[i][0];
    cy = cy + coordline[i][1];
  }
  
  cx=cx/coordline.length;
  cy=cy/coordline.length;
  
  
  for (int i=0; i< coordline.length; i++){
    coordlinepol[i][0] = coordline[i][0] - cx;
    coordlinepol[i][1] = coordline[i][1] - cy;
    coordlinepol[i][2] = coordline[i][2];
    
    float x = coordlinepol[i][0];
    
    coordlinepol[i][0] = (float) Math.sqrt( x * x + coordlinepol[i][1] * coordlinepol[i][1] );
    coordlinepol[i][1] = (float) Math.acos( x / coordlinepol[i][0] );
  }
      
  noStroke();
  
  for (int i=0; i< coordline.length; i++){
     
     int ind=int(coordline[i][2])*40;
     fill(int(colormap[ind][0]), int(colormap[ind][1]), int(colormap[ind][2]),100);
     rect(coordline[i][0], coordline[i][1], 10,10,5);
  }   
}


/*
* Draws a polygon
* @param float x
* @param float y
* @param float radius
* @param int npoint
*/
void polygon(float x, float y, float radius, int npoints) {
  float angle = TWO_PI / npoints;
  beginShape();
  for (float a = 0; a < TWO_PI; a += angle) {
    float sx = x + cos(a) * radius;
    float sy = y + sin(a) * radius;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}
  
