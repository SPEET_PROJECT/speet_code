/*
*
* Copyright (C) 2018 Miguel A. Prada, Universidad de León
* This file (cvDataCube) is part of the SPEET (Student Profile for Enhancing Engineering Tutoring) 
* Project, financed by the European Commission in the framework of the Erasmus+ programme.
*
* cvDataCube is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
*
* cvDataCube is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License.

* The European Commission support for the production of this publication does not constitute 
* an endorsement of the contents which reflects the views only of the authors, and the 
* Commission cannot be held responsible for any use which may be made of the information contained therein
*
*/

'use strict';
// Constants
const TOP_DIR = 10;
const TOTAL_WIDTH = 1000; 
const REGULAR_WIDTH = TOTAL_WIDTH/3;
const SCORE_AVERAGE_WIDTH = TOTAL_WIDTH/1.6;
const MAP_WIDTH = TOTAL_WIDTH/1.4;
const REGULAR_HEIGHT = 150;
const SCORE_AVERAGE_HEIGHT = 220;
const MAP_HEIGHT = 300;
const MAP_SCALE = 500;
const MAP_CENTER_X = 35;
const MAP_CENTER_Y = 41;
//const MAP_COLORS = ["#E2F2FF", "#C4E4FF", "#9ED2FF", "#81C5FF", "#6BBAFF", "#51AEFF", "#36A2FF", "#1E96FF", "#0089FF", "#0061B5"];   // Blue shades 
const MAP_COLORS = ["#c4ffd7", "#9effaf", "#81ffa0", "#6aff8b", "#35ff79", "#1eff7b", "#00ff59", "#00b52d", "#008e23", "#00701b"];     // Green shades
const TOP_MARGIN = 5;
const LEFT_BAR_MARGIN = 45;
const LEFT_ROW_MARGIN = 10;
const RIGHT_MARGIN = 15;
const BOTTOM_MARGIN = 20;
const SA_RIGHT_MARGIN = 10;
const SA_LEFT_MARGIN = 45;
const SA_BOTTOM_MARGIN = 90;
const DATE_FORMAT = d3.time.format('%Y-%m-%d');
const NUMBER_FORMAT = d3.format('.2f');
const X_TICK_NUMBER = 10;
const X_LOW_TICK_NUMBER = 8;
const Y_TICK_NUMBER = 8;
const UNITS_LOW = 30;
const UNITS_HIGH = 40;
const NUMBER_OF_ELEMENTS = 8;
const SEMESTERS = 2;
const DEGREE_YEARS = 4;
const MIN_SCORE = 0;
const MAX_SCORE = 10;
const MAX_RATE = 100;
const MAX_ATTEMPTS = 7;


// Charts
var institutionCountChart;
var sexCountChart;
var scoreCountChart;
var birthyearCountChart;
var lastyearCountChart;
var accesstostudiesCountChart;
var gdpCountChart;
var admissionscoreCountChart;
var subjectnatureCountChart;
var degreenatureCountChart;
var studentCount;
var euChart;
var categoricalChart;
var numericalChart;
var scoreaverageChart;

/*
* Manages data loading and chart updating
* @param {String} path
*/
function loadCSV(path) {

    // Charts are linked to the page elements
    institutionCountChart = dc.rowChart('#institution-count-chart');
    sexCountChart = dc.rowChart('#sex-count-chart');
    scoreCountChart = dc.barChart('#score-count-chart');
    birthyearCountChart = dc.barChart('#birthyear-count-chart');
    lastyearCountChart = dc.barChart('#lastyear-count-chart');
    accesstostudiesCountChart = dc.barChart('#accesstostudies-count-chart');
    gdpCountChart = dc.barChart('#gdp-count-chart');
    admissionscoreCountChart = dc.barChart('#admissionscore-count-chart');
    subjectnatureCountChart = dc.rowChart('#subjectnature-count-chart');
    degreenatureCountChart = dc.rowChart('#degreenature-count-chart');
    studentCount = dc.dataCount('.dc-data-count');
    euChart = dc.geoChoroplethChart("#eu-chart");
    categoricalChart = dc.rowChart('#categorical-chart');
    numericalChart = dc.barChart('#numerical-chart');
    scoreaverageChart = dc.barChart('#scoreaverage-chart');
    $('#content').show();

    // Data are loaded with D3 and charts are built and initialized
    d3.csv(path, function (error, data) {
     
        var maxBirthYear = 2000; 
        var minBirthYear = maxBirthYear;
        var maxLastYear = 2010; 
        var minLastYear = maxLastYear;
        var maxGDP = 100;
        var minGDP = maxGDP;
        var maxNewStudents = 0;
        var maxStudents = 0;
        var maxAccessAge = 18;
        var minAccessAge = maxAccessAge;
        var students = [];

        var selectedDimension1;
        var selectedGroup1;
        var selectedDimension2;
        var selectedGroup2;
        var selectedDimension3;
        var selectedGroup3;
        var isCategorical;
        var min1, min3, max1, max3;

        // Variable mapping
        data.map(function (d) {
                d.institution = d['Institution'];
                d.sex = d['Sex'];
                d.score = +d['SubjectScore'];
                d.birthyear = +d['YearOfBirth'];
                d.lastyear = +d['LastYear'];
                d.accesstostudies = +d['AccessToStudiesAge'];
                d.gdp = +d['GDPpc'];
                d.admissionscore = +d['AdmissionScore'];
                d.subjectnature = d['SubjectNature']; 
                d.subjectmethodology = d['SubjectMethodology']; 
                d.state = d['State']; 
                d.degreenature  = d['DegreeNature'];
                d.numberstudentsfirstyear = +d['NumberStudentsFirstYear'];
                d.numbertotalstudents = +d['NumberTotalStudents'];
                d.subjectaveragescore = +d['SubjectAverageScore'];
                d.subjectfailurerate = +d['SubjectFailureRate'];
                d.subjectnumberattempts = +d['SubjectNumberAttempts'];
                d.subjectyear = +d['SubjectYear'];
                d.subjectsemester = +d['SubjectSemester'];
                d.fathereducationlevel = d['FatherEducationLevel'];
                d.mothereducationlevel = d['MotherEducationLevel'];
                d.mobility = d['Mobility'];
                d.nationality = d['Nationality'];
                d.previousstudies = d['PreviousStudies'];
                d.knowledgearea = d['KnowledgeArea'];

                if ((d.birthyear>0) && (d.birthyear < minBirthYear)) {
                    minBirthYear = d.birthyear;
                }
                if ((d.lastyear>0) && (d.lastyear < minLastYear)) {
                    minLastYear = d.lastyear;
                }
                if ((d.gdp>0) && (d.gdp < minGDP)){
                    minGDP = d.gdp;
                }
                if ((d.accesstostudies>0) && (d.accesstostudies < minAccessAge)){
                    minAccessAge = d.accesstostudies;
                }
                if (d.birthyear > maxBirthYear) {
                    maxBirthYear = d.birthyear;
                }
                if (d.lastyear > maxLastYear) {
                    maxLastYear = d.lastyear;
                }
                if (d.gdp > maxGDP){
                    maxGDP = d.gdp;
                }
                if (d.numberstudentsfirstyear > maxNewStudents){
                    maxNewStudents = d.numberstudentsfirstyear;
                }
                if (d.numbertotalstudents > maxStudents){
                    maxStudents = d.numbertotalstudents;
                }
                if (d.accesstostudies > maxAccessAge){
                    maxAccessAge = d.accesstostudies;
                }

                students.push(d);
        });
        var studentsCube = crossfilter(students);
        var all = studentsCube.groupAll();

        // Dimensions
        var institutionDimension = studentsCube.dimension(function (d) {
            return d.institution ? d.institution : "?";
        });

        var sexDimension = studentsCube.dimension(function (d) {
            return d.sex ? d.sex : "?";
        });

        var scoreDimension = studentsCube.dimension(function (d) {
            return d.score ? d.score : -1;
        });

        var birthyearDimension = studentsCube.dimension(function (d) {
            return d.birthyear ? d.birthyear : -1;
        });

        var lastyearDimension = studentsCube.dimension(function (d) {
            return d.lastyear ? d.lastyear : -1;
        });

        var admissionscoreDimension = studentsCube.dimension(function (d) {
            return d.admissionscore ? Math.round(d.admissionscore*2)/2 : -1;
        });

        var gdpDimension = studentsCube.dimension(function (d) {
            return d.gdp ? d.gdp : -1;
        });

        var accesstostudiesDimension = studentsCube.dimension(function (d) {
            return d.accesstostudies ? d.accesstostudies : -1;
        });

        var stateDimension = studentsCube.dimension(function (d) {
            return d.state ? d.state : "";
        });

        var studentDimension = studentsCube.dimension(function (d) {
            return d['StudentID'] ? d['StudentID'] : 0;
        });

        var subjectnatureDimension = studentsCube.dimension(function (d) {
            return d.subjectnature ? d.subjectnature : "?";
        });

        var subjectmethodologyDimension = studentsCube.dimension(function (d) {
            return d.subjectmethodology ? d.subjectmethodology : "?";
        });

        var degreenatureDimension = studentsCube.dimension(function (d) {
            return d.degreenature ? d.degreenature : "?";
        });

        var fathereducationlevelDimension = studentsCube.dimension(function (d) {
            return d.fathereducationlevel ? d.fathereducationlevel : "?";
        });

        var mothereducationlevelDimension = studentsCube.dimension(function (d) {
            return d.mothereducationlevel ? d.mothereducationlevel : "?";
        });

        var mobilityDimension = studentsCube.dimension(function (d) {
            if(!d.mobility || d.mobility=="")
                return "?";
            else if (parseInt(d.mobility)==1)
                return "Yes";
            else return "No";
        });

        var nationalityDimension = studentsCube.dimension(function (d) {
            return d.nationality ? d.nationality : "?";
        });

        var previousstudiesDimension = studentsCube.dimension(function (d) {
            return d.previousstudies ? d.previousstudies : "?";
        });

        var numberstudentsfirstyearDimension = studentsCube.dimension(function (d) {
            return d.numberstudentsfirstyear ? d.numberstudentsfirstyear : -1;
        });

        var numbertotalstudentsDimension = studentsCube.dimension(function (d) {
            return d.numbertotalstudents ? d.numbertotalstudents : -1;
        });

        var knowledgeareaDimension = studentsCube.dimension(function (d) {
            return d.knowledgearea ? d.knowledgearea : "?";
        });

        var subjectfailurerateDimension = studentsCube.dimension(function (d) {
            return d.subjectfailurerate ? d.subjectfailurerate : -1;
        });

        var subjectnumberattemptsDimension = studentsCube.dimension(function (d) {
            return d.subjectnumberattempts ? d.subjectnumberattempts : -1;
        });

        var subjectyearDimension = studentsCube.dimension(function (d) {
            return d.subjectyear ? d.subjectyear : -1;
        });

        var subjectsemesterDimension = studentsCube.dimension(function (d) {
            return d.subjectsemester ? d.subjectsemester : -1;
        });

        // Groups
        var institutionGroup = institutionDimension.group().reduceCount();
        var sexGroup = sexDimension.group().reduceCount();
        var scoreGroup = scoreDimension.group().reduceCount();
        var birthyearGroup = birthyearDimension.group().reduceCount();
        var lastyearGroup = lastyearDimension.group().reduceCount();
        var accesstostudiesGroup = accesstostudiesDimension.group().reduceCount();
        var gdpGroup = gdpDimension.group().reduceCount();
        var admissionscoreGroup = admissionscoreDimension.group().reduceCount();
        var stateGroup = stateDimension.group().reduceCount();
        var studentGroup = studentDimension.group().reduceCount();
        var subjectnatureGroup = subjectnatureDimension.group().reduceCount();
        var subjectmethodologyGroup = subjectmethodologyDimension.group().reduceCount();
        var degreenatureGroup = degreenatureDimension.group().reduceCount();
        var numberstudentsfirstyearGroup = numberstudentsfirstyearDimension.group().reduceCount();
        var numbertotalstudentsGroup = numbertotalstudentsDimension.group().reduceCount();
        var knowledgeareaGroup = knowledgeareaDimension.group().reduceCount();
        var subjectfailurerateGroup = subjectfailurerateDimension.group().reduceCount();
        var subjectnumberattemptsGroup = subjectnumberattemptsDimension.group().reduceCount();
        var subjectyearGroup = subjectyearDimension.group().reduceCount();
        var subjectsemesterGroup = subjectsemesterDimension.group().reduceCount();
        var fathereducationlevelGroup = fathereducationlevelDimension.group().reduceCount();
        var mothereducationlevelGroup = mothereducationlevelDimension.group().reduceCount();
        var mobilityGroup = mobilityDimension.group().reduceCount();
        var nationalityGroup = nationalityDimension.group().reduceCount();
        var previousstudiesGroup = previousstudiesDimension.group().reduceCount();

        var group1 = admissionscoreDimension.group().reduce(
            function (p, v) {
                ++p.count;
                p.total += v.score;
                p.avg = Math.round(p.total / p.count);
                return p;
            },
            function (p, v) {
                --p.count;
                p.total -=  v.score;
                p.avg = p.count ? Math.round(p.total / p.count) : 0;
                return p;
            },
            function () {
                return {count: 0, total: 0, avg: 0};
            }
        ); 

        // Charts configuration
        institutionCountChart
            .width(REGULAR_WIDTH)
            .height(REGULAR_HEIGHT)
            .margins({top: TOP_MARGIN, left: LEFT_ROW_MARGIN, right: RIGHT_MARGIN, bottom: BOTTOM_MARGIN})
            .dimension(institutionDimension)
            .group(institutionGroup)
            .data(function (group) {
                return group.top(TOP_DIR);
            })
            .ordinalColors(d3.scale.category10().range())
            .title(function(d){return (d.value/institutionDimension.groupAll().reduceCount().value()).toFixed(2) + "";})
            .x(d3.scale.linear().domain([0, 1]))
            .elasticX(true)
            .xAxis().ticks(X_LOW_TICK_NUMBER).tickFormat(d3.format("d"));

        sexCountChart
            .width(REGULAR_WIDTH)
            .height(REGULAR_HEIGHT)
            .margins({top: TOP_MARGIN, left: LEFT_ROW_MARGIN, right: RIGHT_MARGIN, bottom: BOTTOM_MARGIN})
            .dimension(sexDimension)
            .group(sexGroup)
            .data(function (group) {
                return group.top(TOP_DIR);
            })
            .ordinalColors(d3.scale.category10().range())
            .title(function(d){return (d.value/sexDimension.groupAll().reduceCount().value()).toFixed(2) + "";})
            .elasticX(true)
            .xAxis().ticks(X_LOW_TICK_NUMBER).tickFormat(d3.format("d"));

        scoreCountChart
            .dimension(scoreDimension)
            .group(scoreGroup)
            .width(REGULAR_WIDTH)
            .height(REGULAR_HEIGHT)
            .centerBar(true)
            .margins({top: TOP_MARGIN, left: LEFT_BAR_MARGIN, right: RIGHT_MARGIN, bottom: BOTTOM_MARGIN})
            .xUnits(function(){return UNITS_LOW;})
            .x(d3.scale.linear().domain([MIN_SCORE, MAX_SCORE+0.5]))
            .renderHorizontalGridLines(true)
            .filterPrinter(function (filters) {
                var filter = filters[0], s = '';
                s += NUMBER_FORMAT(filter[0]) + ' -> ' + NUMBER_FORMAT(filter[1]);
                return s;
            })
            .elasticY(true)
            .yAxisLabel('Count')
            .xAxis().ticks(X_TICK_NUMBER).tickFormat(d3.format("d"));

        scoreCountChart.xAxis().tickFormat(
            function (v) {
                return v;
            });
        scoreCountChart.yAxis().ticks(Y_TICK_NUMBER); 

        birthyearCountChart
            .dimension(birthyearDimension)
            .group(birthyearGroup)
            .width(REGULAR_WIDTH)
            .height(REGULAR_HEIGHT)
            .centerBar(true)
            .margins({top: TOP_MARGIN, left: LEFT_BAR_MARGIN, right: RIGHT_MARGIN, bottom: BOTTOM_MARGIN})
            .x(d3.scale.linear().domain([minBirthYear, maxBirthYear+1]))
            .renderHorizontalGridLines(true)
            .filterPrinter(function (filters) {
                var filter = filters[0], s = '';
                s += NUMBER_FORMAT(filter[0]) + ' -> ' + NUMBER_FORMAT(filter[1]);
                return s;
            })
            .elasticY(true)
            .yAxisLabel('Count')
            .xAxis().ticks(X_LOW_TICK_NUMBER).tickFormat(d3.format("d"));

        birthyearCountChart.xAxis().tickFormat(
            function (v) {
                return v;
            });
        birthyearCountChart.yAxis().ticks(Y_TICK_NUMBER);

        lastyearCountChart
            .dimension(lastyearDimension)
            .group(lastyearGroup)
            .width(REGULAR_WIDTH)
            .height(REGULAR_HEIGHT)
            .centerBar(true)
            .margins({top: TOP_MARGIN, left: LEFT_BAR_MARGIN, right: RIGHT_MARGIN, bottom: BOTTOM_MARGIN})
            .x(d3.scale.linear().domain([minLastYear, maxLastYear+1]))
            .renderHorizontalGridLines(true)
            .filterPrinter(function (filters) {
                var filter = filters[0], s = '';
                s += NUMBER_FORMAT(filter[0]) + ' -> ' + NUMBER_FORMAT(filter[1]);
                return s;
            })
            .elasticY(true)
            .yAxisLabel('Count')
            .xAxis().ticks(X_LOW_TICK_NUMBER).tickFormat(d3.format("d"));

        lastyearCountChart.xAxis().tickFormat(
            function (v) {
                return v;
            });
        lastyearCountChart.yAxis().ticks(Y_TICK_NUMBER);

        accesstostudiesCountChart
            .dimension(accesstostudiesDimension)
            .group(accesstostudiesGroup)
            .width(REGULAR_WIDTH)
            .height(REGULAR_HEIGHT)
            .x(d3.scale.linear().domain([minAccessAge, maxAccessAge+1]))
            .centerBar(true)
            .margins({top: TOP_MARGIN, left: LEFT_BAR_MARGIN, right: RIGHT_MARGIN, bottom: BOTTOM_MARGIN})
            .renderHorizontalGridLines(true)
            .filterPrinter(function (filters) {
                var filter = filters[0], s = '';
                s += NUMBER_FORMAT(filter[0]) + ' -> ' + NUMBER_FORMAT(filter[1]);
                return s;
            })
            .elasticY(true)
            .yAxisLabel('Count')
            .xAxis().ticks(X_TICK_NUMBER).tickFormat(d3.format("d"));

        accesstostudiesCountChart.xAxis().tickFormat(
            function (v) {
                return v;
            });
        accesstostudiesCountChart.yAxis().ticks(Y_TICK_NUMBER);

        gdpCountChart
            .dimension(gdpDimension)
            .group(gdpGroup)
            .width(REGULAR_WIDTH)
            .height(REGULAR_HEIGHT)
            .centerBar(true)
            .margins({top: TOP_MARGIN, left: LEFT_BAR_MARGIN, right: RIGHT_MARGIN, bottom: BOTTOM_MARGIN})
            .x(d3.scale.linear().domain([minGDP, maxGDP+1]))
            .xUnits(function(){return UNITS_HIGH;})
            .renderHorizontalGridLines(true)
            .filterPrinter(function (filters) {
                var filter = filters[0], s = '';
                s += NUMBER_FORMAT(filter[0]) + ' -> ' + NUMBER_FORMAT(filter[1]);
                return s;
            })
            .elasticY(true)
            .yAxisLabel('Count')
            .xAxis().ticks(X_TICK_NUMBER).tickFormat(d3.format("d"));

        gdpCountChart.xAxis().tickFormat(
            function (v) {
                return v;
            });
        gdpCountChart.yAxis().ticks(Y_TICK_NUMBER);

        admissionscoreCountChart
            .dimension(admissionscoreDimension)
            .group(admissionscoreGroup)
            .width(REGULAR_WIDTH)
            .height(REGULAR_HEIGHT)
            .centerBar(true)
            .margins({top: TOP_MARGIN, left: LEFT_BAR_MARGIN, right: RIGHT_MARGIN, bottom: BOTTOM_MARGIN})
            .xUnits(function(){return UNITS_LOW;})
            .x(d3.scale.linear().domain([MIN_SCORE, MAX_SCORE+0.5]))
            .renderHorizontalGridLines(true)
            .filterPrinter(function (filters) {
                var filter = filters[0], s = '';
                s += NUMBER_FORMAT(filter[0]) + ' -> ' + NUMBER_FORMAT(filter[1]);
                return s;
            })
            .elasticY(true)
            .yAxisLabel('Count')
            .xAxis().ticks(X_TICK_NUMBER).tickFormat(d3.format("d"));

        admissionscoreCountChart.xAxis().tickFormat(
            function (v) {
                return v;
            });
        admissionscoreCountChart.yAxis().ticks(Y_TICK_NUMBER);
      
        subjectnatureCountChart
            .width(REGULAR_WIDTH)
            .height(REGULAR_HEIGHT)
            .margins({top: TOP_MARGIN, left: LEFT_ROW_MARGIN, right: RIGHT_MARGIN, bottom: BOTTOM_MARGIN})
            .dimension(subjectnatureDimension)
            .group(subjectnatureGroup)
            .ordinalColors(d3.scale.category10().range())
            .title(function(d){return (d.value/subjectnatureDimension.groupAll().reduceCount().value()).toFixed(2) + "";})
            .ordering(function (d) {
                return -d.value;
            })
            .elasticX(true)
            .xAxis().ticks(X_LOW_TICK_NUMBER).tickFormat(d3.format("d"));

        degreenatureCountChart
            .width(REGULAR_WIDTH)
            .height(SCORE_AVERAGE_HEIGHT)
            .margins({top: TOP_MARGIN, left: LEFT_ROW_MARGIN, right: RIGHT_MARGIN, bottom: BOTTOM_MARGIN})
            .dimension(degreenatureDimension)
            .group(degreenatureGroup)
            .ordinalColors(d3.scale.category10().range())
            .title(function(d){return (d.value/degreenatureDimension.groupAll().reduceCount().value()).toFixed(2) + "";})
            .ordering(function (d) {
                return -d.value;
            })
            .cap(NUMBER_OF_ELEMENTS)
            .elasticX(true)
            .xAxis().ticks(X_LOW_TICK_NUMBER).tickFormat(d3.format("d"));

        // Customizable charts  
        // Initialization      
        categoricalChart 
            .width(REGULAR_WIDTH)
            .height(REGULAR_HEIGHT)
            .margins({top: TOP_MARGIN, left: LEFT_ROW_MARGIN, right: RIGHT_MARGIN, bottom: BOTTOM_MARGIN})
            .ordinalColors(d3.scale.category10().range())
            .ordering(function (d) {
                return -d.value;
            })
            .cap(NUMBER_OF_ELEMENTS)
            .dimension(subjectmethodologyDimension)
            .group(subjectmethodologyGroup)
            .title(function(d){return (d.value/subjectmethodologyDimension.groupAll().reduceCount().value()).toFixed(2) + "";})
            .xAxis().ticks(X_LOW_TICK_NUMBER).tickFormat(d3.format("d"));

        numericalChart 
            .width(REGULAR_WIDTH)
            .height(REGULAR_HEIGHT)
            .margins({top: TOP_MARGIN, left: LEFT_BAR_MARGIN, right: RIGHT_MARGIN, bottom: BOTTOM_MARGIN})
            .renderHorizontalGridLines(true)
            .centerBar(true)
            .elasticY(true)
            .x(d3.scale.linear().domain([0,DEGREE_YEARS+1]))
            .xUnits(function(){return UNITS_LOW;})  
            .dimension(subjectyearDimension)
            .group(subjectyearGroup);

        scoreaverageChart 
            .width(SCORE_AVERAGE_WIDTH)
            .height(SCORE_AVERAGE_HEIGHT)
            .margins({top: TOP_MARGIN, left: SA_LEFT_MARGIN, right: SA_RIGHT_MARGIN, bottom: SA_BOTTOM_MARGIN})
            .renderHorizontalGridLines(true)
            .elasticY(true)
            .yAxisLabel('Average')
            .x(d3.scale.linear().domain([minBirthYear,maxBirthYear+1]))
            .xUnits(function(){return UNITS_HIGH;})
            .dimension(birthyearDimension)
            .group(birthyearDimension.group().reduce(
                    function (p, v) {
                        ++p.count;
                        p.total += v.score;
                        p.avg = d3.round(p.total / p.count, 1);
                        return p;
                    },
                    function (p, v) {
                        --p.count;
                        p.total -=  v.score;
                        p.avg = p.count ? d3.round(p.total / p.count, 1) : 0;
                        return p;
                    },
                    function () {
                        return {count: 0, total: 0, avg: 0};
                    }
                ) , 'Average of Scores')
            .valueAccessor(function (p) {
                    return p.value.avg;
            });

        // Update variable according to user's selection
        $(document).ready(function(){
            $('#dimension-picker1').on('change', function() {
                switch(this.value) {
                    case 'AdmissionScore':
                        selectedDimension1 = admissionscoreDimension;
                        selectedGroup1 = admissionscoreGroup;
                        min1 = MIN_SCORE;
                        max1 = MAX_SCORE;
                        break;
                    case 'NumberStudentsFirstYear':
                        selectedDimension1 = numberstudentsfirstyearDimension;
                        selectedGroup1 = numberstudentsfirstyearGroup;
                        min1 = 0;
                        max1 = maxNewStudents;
                        break;
                    case 'NumberTotalStudents':
                        selectedDimension1 = numbertotalstudentsDimension;
                        selectedGroup1 = numbertotalstudentsGroup;
                        min1 = 0;
                        max1 = maxStudents;
                        break;
                    case 'SubjectFailureRate':
                        selectedDimension1 = subjectfailurerateDimension;
                        selectedGroup1 = subjectfailurerateGroup;
                        min1 = 0;
                        max1 = MAX_RATE+1;
                        break;
                    case 'SubjectNumberAttempts':
                        selectedDimension1 = subjectnumberattemptsDimension;
                        selectedGroup1 = subjectnumberattemptsGroup;
                        min1 = 0;
                        max1 = MAX_ATTEMPTS+1;
                        break;
                    case 'SubjectYear':
                        selectedDimension1 = subjectyearDimension;
                        selectedGroup1 = subjectyearGroup;
                        min1 = 0;
                        max1 = DEGREE_YEARS+1;
                        break;
                    case 'SubjectSemester':
                        selectedDimension1 = subjectsemesterDimension;
                        selectedGroup1 = subjectsemesterGroup;
                        min1 = 0;
                        max1 = SEMESTERS+1;
                        break;
               }

                numericalChart.x(d3.scale.linear().domain([min1,max1]))
                .dimension(selectedDimension1)
                .group(selectedGroup1);

                numericalChart.render();   
         
            }); 


            $('#dimension-picker2').on('change', function() {
                switch(this.value) {
                    case 'SubjectMethodology':
                        selectedDimension2 = subjectmethodologyDimension;
                        selectedGroup2 = subjectmethodologyGroup;
                        break;
                    case 'FatherEducationLevel':
                        selectedDimension2 = fathereducationlevelDimension;
                        selectedGroup2 = fathereducationlevelGroup;
                        break;
                    case 'MotherEducationLevel':
                        selectedDimension2 = mothereducationlevelDimension;
                        selectedGroup2 = mothereducationlevelGroup;
                        break;
                    case 'Mobility':
                        selectedDimension2 = mobilityDimension;
                        selectedGroup2 = mobilityGroup;
                        break;
                    case 'Nationality':
                        selectedDimension2 = nationalityDimension;
                        selectedGroup2 = nationalityGroup;
                        break;
                    case 'PreviousStudies':
                        selectedDimension2 = previousstudiesDimension;
                        selectedGroup2 = previousstudiesGroup;
                        break;
                    case 'KnowledgeArea':
                        selectedDimension2 = knowledgeareaDimension;
                        selectedGroup2 = knowledgeareaGroup;
                        break;
               }

                categoricalChart
                .dimension(selectedDimension2)
                .group(selectedGroup2)
                .ordinalColors(d3.scale.category10().range())
                .title(function(d){return (d.value/selectedDimension2.groupAll().reduceCount().value()).toFixed(2) + "";})
                .xAxis().ticks(X_LOW_TICK_NUMBER).tickFormat(d3.format("d"));

                categoricalChart.render();   

            }); 


            $('#dimension-picker3').on('change', function() {
                switch(this.value) {
                    case 'BirthYear':
                        selectedDimension3 = birthyearDimension;
                        isCategorical = false;
                        min3 = minBirthYear;
                        max3 = maxBirthYear+1;
                        break;
                    case 'LastYear':
                        selectedDimension3 = lastyearDimension;
                        isCategorical = false;
                        min3 = minLastYear;
                        max3 = maxLastYear+1;
                        break;
                    case 'GDPpc':
                        selectedDimension3 = gdpDimension;
                        isCategorical = false;
                        min3 = minGDP;
                        max3 = maxGDP+5;
                        break;
                    case 'AccessToStudies':
                        selectedDimension3 =accesstostudiesDimension;
                        isCategorical = false;
                        min3 = minAccessAge;
                        max3 = maxAccessAge+1;
                        break;
                    case 'AdmissionScore':
                        selectedDimension3 = admissionscoreDimension;
                        isCategorical = false;
                        min3 = MIN_SCORE;
                        max3 = MAX_SCORE+0.5;
                        break;
                    case 'NumberStudentsFirstYear':
                        selectedDimension3 = numberstudentsfirstyearDimension;
                        isCategorical = false;
                        min3 = 0;
                        max3 = maxNewStudents+1;
                        break;
                    case 'SubjectFailureRate':
                        selectedDimension3 = subjectfailurerateDimension;
                        isCategorical = false;
                        min3 = 0;
                        max3 = MAX_RATE+1;
                        break;
                    case 'SubjectNumberAttempts':
                        selectedDimension3 = subjectnumberattemptsDimension;
                        isCategorical = false;
                        min3 = 0;
                        max3 = MAX_ATTEMPTS+1;
                        break;
                    case 'SubjectYear':
                        selectedDimension3 = subjectyearDimension;
                        isCategorical = false;
                        min3 = 0;
                        max3 = DEGREE_YEARS+1;
                        break;
                    case 'SubjectSemester':
                        selectedDimension3 = subjectsemesterDimension;
                        isCategorical = false;
                        min3 = 0;
                        max3 = SEMESTERS+1;
                        break;
                    case 'Sex':
                        selectedDimension3 = sexDimension;
                        isCategorical = true;
                        break;
                    case 'Institution':
                        selectedDimension3 = institutionDimension;
                        isCategorical = true;
                        break;
                    case 'SubjectNature':
                        selectedDimension3 = subjectnatureDimension;
                        isCategorical = true;
                        break;
                    case 'DegreeNature':
                        selectedDimension3 = degreenatureDimension;
                        isCategorical = true;
                        break;
                    case 'SubjectMethodology':
                        selectedDimension3 = subjectmethodologyDimension;
                        isCategorical = true;
                        break;
                    case 'FatherEducationLevel':
                        selectedDimension3 = fathereducationlevelDimension;
                        isCategorical = true;
                        break;
                    case 'MotherEducationLevel':
                        selectedDimension3 = mothereducationlevelDimension;
                        isCategorical = true;
                        break;
                    case 'Mobility':
                        selectedDimension3 = mobilityDimension;
                        isCategorical = true;
                        break;
                    case 'Nationality':
                        selectedDimension3 = nationalityDimension;
                        isCategorical = true;
                        break;
                    case 'PreviousStudies':
                        selectedDimension3 = previousstudiesDimension;
                        isCategorical = true;
                        break;
                    case 'KnowledgeArea':
                        selectedDimension3 = knowledgeareaDimension;
                        isCategorical = true;
                        break;
               }

               // Compute average grouping
                var scoreaverageGroup = selectedDimension3.group().reduce(
                    function (p, v) {
                        ++p.count;
                        p.total += v.score;
                        p.avg = d3.round(p.total / p.count, 1);
                        return p;
                    },
                    function (p, v) {
                        --p.count;
                        p.total -=  v.score;
                        p.avg = p.count ? d3.round(p.total / p.count, 1) : 0;
                        return p;
                    },
                    function () {
                        return {count: 0, total: 0, avg: 0};
                    }
                ); 

                // Define chart depending of the variable type
                if(isCategorical){
                    scoreaverageChart
                    .x(d3.scale.ordinal())
                    .xUnits(dc.units.ordinal) 
                    .dimension(selectedDimension3)
                    .group(scoreaverageGroup, 'Average of Scores')
                    .valueAccessor(function (p) {
                        return p.value.avg;
                    });
                }
                else{
                    scoreaverageChart
                    .x(d3.scale.linear().domain([min3,max3]))
                    .xUnits(function(){return UNITS_HIGH;})
                    .dimension(selectedDimension3)
                    .group(scoreaverageGroup, 'Average of Scores')
                    .brushOn(true)
                    .valueAccessor(function (p) {
                        return p.value.avg;
                    });
                } 

                scoreaverageChart.render();      
            });
        });

         
        // Total count and reset
        studentCount 
            .dimension(studentsCube)
            .group(all)
            .html({
                some: '<strong>%filter-count</strong> selected out of <strong>%total-count</strong> records' +
                ' | <a href=\'javascript:dc.filterAll(); dc.renderAll();\'\'>Reset All</a>',
                all: 'All records selected. Please click on the graph to apply filters.'
            });

        // Choropleth chart
        d3.json("./nuts2", function (statesJson) {
            euChart.width(MAP_WIDTH)
                    .height(MAP_HEIGHT)
                    .dimension(stateDimension)
                    .group(stateGroup)
                    //.projection(d3.geo.mercator().scale(MAP_SCALE).center([MAP_CENTER_X, MAP_CENTER_Y]))  // Mercator projection (narrower)
                    .projection(d3.geo.equirectangular().scale(MAP_SCALE).center([MAP_CENTER_X, MAP_CENTER_Y])) // Equirectangular projection
                    .colors(d3.scale.quantize().range(MAP_COLORS))
                    .colorDomain([0, 200])
                    .overlayGeoJson(statesJson.features, "state", function (d) {
                        return d.properties.name;
                    })
                    .valueAccessor(function(kv) {
                        console.log(kv);
                        return kv.value;
                    })
                    .title(function (d) {
                        return "State: " + d.key + "\nNumber of records: " + d3.format("d")(d.value ? d.value : 0) + "\nPercentage: " + d3.format(".2f")(d.value ? (100*d.value/stateDimension.groupAll().reduceCount().value()) : 0);
                    });
            dc.renderAll();
        });

        $('#loading-image').fadeOut('slow'); 
       // dc.renderAll();


    });
}
